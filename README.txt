PageBuilderScrape.js saves Luminate PageBuilder content from the Preview link in the PageBuilder page list. It runs in the iMacros add-on for Firefox. It should run in the IE plugin as well, but this is untested. Though this is JavaScript, it can only run in iMacros since there are calls to iMacros proprietary functions.

Requirements:

Firefox
https://www.mozilla.org/en-US/firefox/new/

iMacros add-on for Firefox
https://addons.mozilla.org/en-US/firefox/addon/imacros-for-firefox/


Installation:

Copy PageBuilderScrape.js to the macro directory in your home directory. On Mac and Linux that is here:

~/iMacros/Macros/


Running:

1. Log in to the client's instance of Luminate in Firefox.
2. Browse to PageBuilderAdmin (Content > PageBuilder).
3. Open the iMacros sidebar and locate PageBuilderScrape.js.
4. Double-click on PageBuilderScrape.js or click the Play button.
5. Go get coffee.

The script has a one second wait between page saves. This reduces load on the server and should prevent Luminate IT from banning you.
