var looping         = true,
    indicator       = 1,
    clickPrevTop    = 'CODE:' +
                      'TAB T=1\n' +
                      'TAG POS=',
    clickPrevBottom = ' TYPE=A ATTR=TXT:Preview',
    saveAndClose    = 'TAB T=2\n' +
                      'SET pageName EVAL("\'{{!URLCURRENT}}\'.split(\'pagename=\')[1]")\n' +
                      'SAVEAS TYPE=HTM FOLDER=/Users/pauljoiner/TWS435Pages/ FILE={{pageName}}.html\n' +
                      'TAB CLOSE\n' +
                      'TAB T=1\n' +
                      'WAIT SECONDS=1';

while (looping) {

	for (var counter = 1; counter < 21; counter++) {
		indicator = iimPlay('CODE: TAG POS=' + counter + ' TYPE=A ATTR=TXT:Preview EXTRACT=HREF');

		if (indicator > 0) {
			var pageURL  = iimGetLastExtract(1),
			    pageName = pageURL.split('pagename=')[1],
			    macro    = 'CODE: ' +
			               'TAB OPEN\n' +
			               'TAB T=2\n' +
			               'URL GOTO=' + pageURL + '\n' +
			               'SAVEAS TYPE=HTM FOLDER=/Users/pauljoiner/TWS435Pages/ FILE=' + pageName + '.html\n' +
			               'TAB CLOSE\n' +
			               'WAIT SECONDS=1';

			indicator = iimPlay(macro);

			if (indicator < 0) {
				errtext = iimGetLastError();
				iimDisplay(errtext + '\ncounter = ' + counter);
				break;
			} // end if there's an error

		} else {
			// if there's an error
			errtext = iimGetLastError();
			iimDisplay(errtext);
			break;
		}
	} // end loop through PB page list

	// shall we continue?
	indicator = iimPlay('CODE: TAG POS=1 TYPE=P ATTR=ID:lc_pageCount EXTRACT=TXT');

	if (indicator > 0) {
		var positionString      = iimGetLastExtract(1),
		    [thisPage, endPage] = positionString.split(' - ')[1].split(' of ');
		indicator = iimPlay('CODE: TAG POS=1 TYPE=A ATTR=TXT:Next');
		looping = !(thisPage == endPage);

	} else {
		// if there's an error
		errtext = iimGetLastError();
		iimDisplay(errtext);
	}// end if pagePosition was successful

} // end loop